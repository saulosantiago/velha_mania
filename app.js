require('dot-env')

var express = require('express'),
    app = express(),
    load = require('express-load'),
    server = require('http').createServer(app),
    error = require('./middleware/errors'),
    io = require('socket.io').listen(server),
    redis = require('./middleware/redis_connect'),
    ExpressStore = redis.getExpressStore(),
    SocketStore = redis.getSocketStore(),
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    beforeAuthentication = require('./middleware/authenticator');

require('./middleware/facebook-passport')(passport, FacebookStrategy)
const KEY = 'velha_mania.sid', SECRET = 'velha_mania', MAX_AGE = {
    maxAge: 3600000
}, GZIP_LVL = {
    level: 9,
    memLevel: 9
}, PORT = 3000;

var cookie = express.cookieParser(SECRET),
    storeOpts = {
        client: redis.getClient(),
        prefix: KEY
    }, store = new ExpressStore(storeOpts),
    sessionOptions = {
        secret: SECRET,
        key: KEY,
        store: store
    }, session = express.session(sessionOptions);

app.use(express.logger('dev'));

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(cookie);
app.use(session);
app.use(passport.initialize());
app.use(passport.session());
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.compress(GZIP_LVL));
app.use(app.router);
// app.use(express.static(__dirname + '/public', MAX_AGE));
app.use(error.notFound);
app.use(error.serverError);

io.enable('browser client cache')
io.enable('browser client minification')
io.enable('browser client etag')
io.set('log level', 1)
io.set('authorization', function(data, accept) {
    cookie(data, {}, function(err) {
        var sessionID = data.signedCookies[KEY];
        store.get(sessionID, function(err, session) {
            if (err || !session) {
                accept(null, false);
            } else {
                data.session = session;
                accept(null, true);
            }
        });
    });
});

load('models')
    .then('controllers')
    .then('routes')
    .into(app, beforeAuthentication);

load('sockets')
    .into(io);

server.listen(PORT)
module.exports = app;