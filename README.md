Velha mania(em construção)
---

Jogo da velha multiplayer utilizando a poderosa ferramenta....NODE JS.

Get Started:

- npm install -g grunt-cli
- npm install -g bower
- npm install
- bower install
- npm start

Run tasks:

- grunt watch

## Configs Server

# root
- apt-get update
- apt-get upgrade
- apt-get dist-upgrade
- apt-get install build-essential
- adduser velha-mania
- sudo /usr/sbin/visudo
  velha-mania ALL=NOPASSWD:ALL
  blog        ALL=NOPASSWD:ALL

# user velha-mania

- mkdir ~/.ssh
- vim ~/.ssh/authorized_keys
- chmod 700 ~/.ssh
- chmod 600 ~/.ssh/authorized_keys
- sudo apt-get install ufw
- sudo ufw default deny
- sudo ufw logging on
- sudo ufw logging low
- sudo ufw allow ssh
- sudo ufw allow www
- sudo ufw allow https
- sudo ufw limit ssh
- sudo ufw enable
- sudo reload ssh
- ssh-keygen -t rsa -C "velha-mania"
- add a bitbucket
- add a github
- ssh -T git@bitbucket.org
- ssh -T git@github.com

- sudo apt-get install denyhosts
- sudo apt-get install unattended-upgrades
- sudo vim /etc/apt/apt.conf.d/10periodic
-
  APT::Periodic::Update-Package-Lists "1";
  APT::Periodic::Download-Upgradeable-Packages "1";
  APT::Periodic::AutocleanInterval "7";
  APT::Periodic::Unattended-Upgrade "1";

- sudo apt-get install ntp
- sudo dpkg-reconfigure tzdata

- [mongo](https://www.digitalocean.com/community/articles/how-to-install-mongodb-on-ubuntu-12-04)

- redis
  cd /tmp
  wget http://redis.googlecode.com/files/redis-2.4.16.tar.gz
  tar xzf redis-2.4.16.tar.gz
  cd redis-2.4.16
  make
  make test
  sudo make install
  cd utils
  sudo ./install_server.sh
  sudo update-rc.d redis_6379 defaults

- [nginx](https://www.digitalocean.com/community/articles/how-to-install-nginx-on-ubuntu-12-04-lts-precise-pangolin)

- sudo apt-get install git-core
- curl https://raw.githubusercontent.com/creationix/nvm/v0.10.0/install.sh | sh
- reboot
- nvm install 0.10.10
- nvm alias default 0.10.10
- source ~/.nvm/nvm.sh
- curl http://npmjs.org/install.sh -L -o -| sh

- npm install -g grunt-cli
- npm install -g bower
- sudo vim /etc/environment
 export CI=true

- sudo vim /etc/ssh/sshd_config
 AllowUsers root velha-mania blog

- sudo nano /etc/monit/monitrc
- monit reload

- sudo vim /etc/init/velha-mania.conf(copy and paste file)
- sudo vim /etc/mongodb.conf
  use admin
  db.addUser( { user: "admin", pwd: "admin-22/03/2014", roles: [ "dbAdminAnyDatabase" ] } )

- sudo apt-get install postfix
- sudo nano /etc/postfix/main.cf
  myhostname = example.com
  virtual_alias_maps = hash:/etc/postfix/virtual
- sudo /etc/init.d/postfix reload
- sudo nano /etc/postfix/virtual
  foo@bar.com
- sudo postmap /etc/postfix/virtual


# user blog

- curl -L get.rvm.io | bash -s stable
- source ~/.rvm/scripts/rvm
- rvm requirements
- rvm install 2.1.1
- sudo apt-get install mysql-server
- sudo mysql_secure_installation
- SELECT User,Host,Password FROM mysql.user;
- mysql> CREATE database blog_production;
- mysql> CREATE USER 'blog'@'localhost' IDENTIFIED BY 'password'
- mysql> GRANT ALL ON blog_production.* TO 'blog_production'@'localhost';
- mysql> FLUSH PRIVILEGES;
- npm install nib