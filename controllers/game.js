module.exports = function(app) {
    var User = app.models.user,
        _ = require('underscore');

    var GameController = {
        index: function(req, res) {
            User.find({
                nickname: req.body.data.itsMe
            }, function(error, user) {
                var data = req.body.data,
                    user = user[0],
                    disputes = user.disputes,
                    dispute = undefined;

                dispute = _.where(disputes, {
                    opponent: data.opponent
                })
                dispute = _.first(dispute)

                if (_.isEmpty(dispute)) {
                    disputes.push(_.omit(data, 'itsMe'))
                } else {
                    if (dispute.opponent == data.opponent) {
                        dispute.win = parseInt(dispute.win) + parseInt(data.win)
                        dispute.lose = parseInt(dispute.lose) + parseInt(data.lose)
                        dispute.draw = parseInt(dispute.draw) + parseInt(data.draw)
                        dispute.matches = parseInt(dispute.matches) + parseInt(data.matches)
                    }
                }

                user.save(function(err, user) {
                    res.json(true)
                });
            });
        },

        stats: function(req, res) {
            User.find({
                nickname: req.body.itsMe
            }, function(error, user) {
                var data = req.body,
                    user = user[0],
                    disputes = user.disputes;

                _.each(disputes, function(dispute) {
                    if (dispute.opponent == data.opponent) {
                        res.json({
                            disputes: dispute
                        })
                    }
                })


            })
        }
    }

    return GameController
}