module.exports = function(app) {

  var CjAuthenticationController = {
    callback: function (req, res) {
      res.render('clickjogos/callback');
    },

    authentication: function(req, res) {
      req.session.user = req.body.user
      res.json({ status: true, message: 'success' })
    }
  }

  return CjAuthenticationController
}