function FacebookCallback(app, req, res) {
    this.User = app.models.user;
    this.req = req;
    this.res = res;
    this._ = require('underscore');
}

FacebookCallback.prototype._queriesAuthFacebook = function(auth) {
    return '?nickname=' + auth._json.name +
        '&gender=' + auth._json.gender +
        '&avatar=' + 'https://graph.facebook.com/' + auth.username + '/picture' +
        '&providers_name=' + auth.provider +
        '&providers_uid=' + auth._json.id
}

FacebookCallback.prototype.verify = function(auth) {
    this.User.findOne({
        'providers.uid': auth.id
    })
        .exec(function(error, result) {
            if (result) {
                result = this._.pick(result, 'nickname', 'email', 'avatar')
                this.req.session.user = result
                this.res.redirect('/facebook/authenticated/')
            } else {
                var queries = this._queriesAuthFacebook(auth)
                this.res.redirect('/callback/facebook/' + queries)
            }
        }.bind(this));
}

module.exports = function(app) {

    var FacebookAuthenticationController = {
        auth: function(req, res) {
            callback = new FacebookCallback(app, req, res)
            callback.verify(req.user)
        },

        callback: function(req, res) {
            res.render('facebook/callback')
        },

        authentication: function(req, res) {
            req.session.user = req.body.user
            res.json({
                status: true,
                message: 'success'
            })
        },

        authenticated: function(req, res) {
            res.render('facebook/authenticated')
        }
    }

    return FacebookAuthenticationController
}