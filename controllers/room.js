module.exports = function(app) {

  var RoomController = {
    index: function(req, res) {
      res.render('room/index');
    }
  };

  return RoomController;
};