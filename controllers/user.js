function User (app, res) {
  this.User = app.models.user;
  this._ = require('underscore');
  this.res = res;
}

User.prototype.verify = function (user) {
  this.User.findOne({ email: user.email })
    .exec(function(error, result){
      if (result) {
        this._providerSave(user, result)
      } else {
        this._userSave(user)
      }
    }.bind(this));
}

User.prototype._providerSave = function (user, result) {
  var providers = result.providers
    , isProvider = this._.where(providers, user.providers);

  if (this._.isEmpty(isProvider)) {
    providers.push(user.providers)
    result.save(function (err, result) {
      if (err) {
        this.res.json({ status: false, message: 'error in subscribe' })
      }
    }.bind(this))
  }

  result = this._.pick(result, 'nickname', 'email', 'avatar')
  this.res.json({ status: true, message: 'success', result: result })
}

User.prototype._userSave = function (user) {
  user.providers = [user.providers]

  this.User.create(user, function(error, result) {
    if(error){
      this.res.json({ status: false, message: 'error in subscribe' })
    } else {
      result = this._.pick(result, 'nickname', 'email', 'avatar')
      this.res.json({ status: true, message: 'success', result: result })
    }
  }.bind(this));
}

module.exports = function(app) {
  var UserController = {
    save: function (req, res) {
      user = new User(app, res)
      user.verify(req.body.user)
    }
  }

  return UserController
}