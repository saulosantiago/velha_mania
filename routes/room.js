module.exports = function(app, beforeAuthentication) {
    var room = app.controllers.room;
    app.get('/sala/', beforeAuthentication, room.index);
    app.get('/en/sala/', beforeAuthentication, room.index);
};