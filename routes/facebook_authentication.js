module.exports = function(app) {
    var facebook = app.controllers.facebookAuthentication,
        passport = require('passport');

    app.get('/auth/facebook/', passport.authenticate('facebook'));

    app.get('/auth/facebook/callback/', passport.authenticate('facebook', {
        failureRedirect: '/'
    }), facebook.auth);

    app.get('/callback/facebook/', facebook.callback);
    app.post('/facebook/authentication/', facebook.authentication);
    app.get('/facebook/authenticated/', facebook.authenticated);
};