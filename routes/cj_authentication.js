module.exports = function(app) {
    var cj = app.controllers.cjAuthentication;
    app.get('/callback/cj/', cj.callback);
    app.post('/cj/authentication/', cj.authentication);
};