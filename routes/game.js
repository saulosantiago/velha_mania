module.exports = function(app) {
    var game = app.controllers.game;
    app.post('/save-game-details/', game.index);
    app.post('/stats-game/', game.stats);
};