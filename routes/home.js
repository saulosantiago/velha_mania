module.exports = function(app, beforeAuthentication) {
    var home = app.controllers.home;
    app.get('/', beforeAuthentication, home.index);
    app.get('/en/', beforeAuthentication, home.index);
};