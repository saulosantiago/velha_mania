var grunt = require('grunt'),
    _ = grunt.util._;

var sprite = require('node-sprite'),
    fs = require('fs'),
    jsons = undefined,
    imagesExists = undefined,
    glob = require('glob'),
    jsons = glob.sync('./public/images/*.json'),
    images = glob.sync('./public/images/*.png');

var removeImages = function() {
    if (!_.isEmpty(images)) {
        images.forEach(function(image) {
            fs.unlink(image, function(err) {
                if (err) {
                    console.log(err)
                }
            })
        })
    }
}

var removeJsons = function() {
    if (!_.isEmpty(jsons)) {
        jsons.forEach(function(json) {
            fs.unlink(json, function(err) {
                if (err) {
                    console.log(err)
                }
            })
        })
    }
}

var generateSprite = function() {
    sprite.sprites({
        path: './public/images'
    }, function(err, globalSprite) {
        if (err) {
            console.log(err)
        } else {
            console.log('has been created!')
            runTask()
        }
    });
}

var runTask = function() {
    grunt.tasks(['node_sprite_mixings'], {}, function() {
        grunt.log.ok('Done running tasks.');
    });
}

removeImages()
removeJsons()
generateSprite()