const FACEBOOK_APP_ID = process.env.FACEBOOK_APP_ID,
FACEBOOK_APP_SECRET = process.env.FACEBOOK_APP_SECRET,
DOMAIN = process.env.DOMAIN;

module.exports = function(passport, FacebookStrategy) {
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(obj, done) {
        done(null, obj);
    });

    passport.use(new FacebookStrategy({
            clientID: FACEBOOK_APP_ID,
            clientSecret: FACEBOOK_APP_SECRET,
            callbackURL: DOMAIN + '/auth/facebook/callback/'
        },
        function(accessToken, refreshToken, profile, done) {
            process.nextTick(function() {
                return done(null, profile);
            });
        }
    ));
}