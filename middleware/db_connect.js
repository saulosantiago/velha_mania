var mongoose = require('mongoose'),
    singleConnection;

module.exports = function() {
    var url = process.env.MONGO_URL;
    if (!singleConnection) {
        singleConnection = mongoose.connect(url);
    }
    return singleConnection;
}