module.exports = function(req, res, next) {
    if (req.url != '/' && !req.session.user) {
        return res.redirect('/');
    }

    if (req.url == '/' && req.session.user) {
        return res.redirect('sala/');
    }

    return next();
};