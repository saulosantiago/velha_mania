module.exports = function(app) {
  var db = require("../middleware/db_connect")();
  var Schema = require('mongoose').Schema;

  var dispute = Schema({
      matches: String
    , win: String
    , lose: String
    , draw: String
    , opponent: String
  });

  var provider = Schema({
      name: String
    , uid: String
  });

  var user = Schema({
    nickname: { type: String, required: true }
    , email: { type: String, required: true, index: { unique: true }}
    , gender: { type: String }
    , birthdate: { type: String }
    , avatar: { type: String }
    , confirmatedAt: { type: Date, default: null }
    , disputes: [dispute]
    , providers: [provider]
  })

  user.index({ nickname: 1, email: 1 })
  provider.index({ uid: 1 })
  dispute.index({ opponent: 1 })
  return db.model('user', user);
};