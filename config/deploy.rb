lock '3.1.0'

set :application, 'velha-mania'
set :repo_url, 'git@bitbucket.org:saulosantiago/velha_mania.git'
set :branch, 'master'

set :deploy_to, '/home/velha-mania/www/'
set :scm, :git

set :format, :pretty
set :log_level, :debug
set :pty, true
set :keep_releases, 5

set :nvm_path, '/home/velha-mania/.nvm'
set :nvm_node, 'v0.10.10'
set :nvm_map_bins, %w{node npm grunt bower}

set :grunt_file, -> { release_path.join('Gruntfile.js') }
set :grunt_tasks, %w{ymlObserver cssObserver jsObserver}

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute <<-CMD
        sudo stop velha-mania
        sudo start velha-mania
      CMD
    end
  end

  after 'npm:install', :copy_env do
    on roles(:app) do
      execute <<-CMD
        chmod +x #{ release_path }/bin/velha_mania
        cp #{ deploy_to }shared/.env.json #{ release_path }/.env.json
      CMD
    end
  end

  after 'bower:install', 'grunt'
  after :publishing, :restart
end
