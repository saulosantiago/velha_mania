module.exports = function(grunt) {
    var config = {};
    var plugins = [
        "grunt-contrib-uglify", "grunt-contrib-concat", "grunt-contrib-watch",
        "grunt-contrib-handlebars", "grunt-yaml", "grunt-merge-json",
        "grunt-json", "grunt-contrib-stylus",
        "grunt-contrib-cssmin", "grunt-img", "grunt-node-sprite-mixings"
    ];

    // Watch ================================================
    config.watch = {};

    config.watch.css = {
        files: ["public/fonts/**/*.css", "public/stylus/**/*.styl"],
        tasks: ["cssObserver"]
    };

    config.watch.yml = {
        files: ["locales/**/*.yml"],
        tasks: ["ymlObserver"]
    };

    config.watch.js = {
        files: [
            "public/javascripts/templates/**/*.hbs",
            "public/javascripts/locales/locale.js",
            "public/javascripts/velha_mania/**/*.js",
            "!public/javascripts/velha_mania.*.js",
            "!public/javascripts/velha_mania.js"
        ],
        tasks: ["jsObserver"]
    };

    config.watch.images = {
        files: ["public/images/img/*.png"],
        tasks: ['imagesObserver']
    };

    // Handlebars ===========================================
    config.handlebars = {};

    config.handlebars.jsObserver = {
        options: {
            namespace: "VelhaMania.templates",
            processName: function(file) {
                return file.replace(/^public\/javascripts\/templates\/html\/(.*?)\.hbs$/, "$1");
            }
        }

        ,
        files: {
            "public/javascripts/templates/templates.js": ["public/javascripts/templates/html/**/*.hbs"]
        }
    };

    // Yml ==================================================
    config.yaml = {};

    config.yaml.options = {
        ignored: /^_/,
        space: 4,
        constructors: {
            '!include': function(node, yaml) {
                var data = grunt.file.read(node.value, 'utf-8');
                return yaml.load(data);
            }
        }
    };

    config.yaml.ymlObserver = {
        files: [{
            expand: true,
            cwd: 'locales/',
            src: ['**/*.yml'],
            dest: 'public/javascripts/locales/'
        }]
    };

    // Json merge ===========================================
    config['merge-json'] = {
        "en": {
            src: ["public/javascripts/locales/en/*-en.json"],
            dest: "public/javascripts/locales/en.json"
        },
        "ptBR": {
            src: ["public/javascripts/locales/ptBR/*-ptBR.json"],
            dest: "public/javascripts/locales/ptBR.json"
        },
    }

    // create locale js en and ptBR ============================
    config.json = {
        main: {
            options: {
                namespace: ['Locale'],
                includePath: false,
            },
            src: ['public/javascripts/locales/*.json'],
            dest: 'public/javascripts/locales/locale.js'
        },
    }

    // Concat ===============================================
    config.concat = {};
    config.concat.options = {
        separator: ";"
    };

    config.concat.jsObserver = {
        src: [
            "public/javascripts/components/jquery/dist/jquery.js",
            "public/javascripts/components/module/module.js",
            "public/javascripts/components/handlebars/handlebars.runtime.js",
            "public/javascripts/components/emitter/emitter.js",
            "public/javascripts/components/underscore/underscore.js",
            "public/javascripts/components/jquery.maskedinput/jquery.maskedinput.js",
            "public/javascripts/components/buzz/dist/buzz.js",
            "public/javascripts/locales/locale.js",
            "public/javascripts/templates/**/*.js",
            "public/javascripts/velha_mania/**/*.js"
        ],
        dest: "public/javascripts/velha_mania.development.js"
    };

    // Uglify ===============================================
    config.uglify = {};

    config.uglify.options = {
        sourceMap: "public/javascripts/velha_mania.map.js",
        sourceMappingURL: "/javascripts/velha_mania.map.js",
        sourceMapPrefix: 2,
        beautify: true
        //, report: "gzip"
    };

    config.uglify.jsObserver = {
        src: "public/javascripts/velha_mania.development.js",
        dest: "public/javascripts/velha_mania.js"
    };

    // Stylus ================================================
    config.stylus = {}

    config.stylus.compile = {
        options: {
            paths: ['public/stylus/mixings'],
            use: [
                require('nib')
            ],
            import: ['settings', 'img', 'nib']
        },
        files: {
            'public/stylesheets/velha_mania.css': ['public/stylus/*.styl', 'public/fonts/**/*.css']
        }
    };

    config.stylus.cssObserver = {
        options: {
            spawn: false
        }
    }

    // Css minify ==============================================
    config.cssmin = {}

    config.cssmin.minify = {
        expand: true,
        cwd: 'public/stylesheets/',
        src: ['*.css', '!*.min.css'],
        dest: 'public/stylesheets/',
        ext: '.min.css'
    }

    // Images compressor ========================================
    config.img = {}

    config.img.imagesObserver = {
        src: 'public/images/img/*.png'
    }

    // Generate mixing ==========================================
    config.node_sprite_mixings = {}

    config.node_sprite_mixings = {
        files: {
            dest: 'public/stylus/mixings/img.styl',
            src: ['public/images/*.json']
        },
        options: {
            jsonRemove: true,
            urlNamespace: '/images/'
        }
    }

    // Project configuration.
    grunt.initConfig(config);

    // Load all plugins.
    plugins.forEach(grunt.loadNpmTasks);

    // Task default
    grunt.registerTask("default", ["ymlObserver", "jsObserver", "cssObserver"]);

    // Task
    grunt.registerTask("imagesObserver", ["img"]);
    grunt.registerTask("jsObserver", ["handlebars", "concat", "uglify"]);
    grunt.registerTask("cssObserver", ["stylus", "cssmin"]);
    grunt.registerTask("ymlObserver", ["yaml", "merge-json", "json"]);
};