var users = [],
    _ = require('underscore');

function Game(sockets, redis, user) {
    this.sockets = sockets;
    this.redis = redis;
    this.user = user;
    this.champion = undefined;
}

Game.prototype.played = function(options) {
    this.sockets.clients(options.room).forEach(function(element) {
        element.get('nickname', function(err, result) {
            if (result == this.user.nickname) {
                element.emit('locked', false)
            } else {
                element.emit('unlocked', {
                    status: true,
                    position: options.position,
                    type: options.type
                })
            }
        }.bind(this))
    }.bind(this))
}

Game.prototype.win = function(options) {
    this.champion = this.user.nickname
    this.sockets.clients(options.room).forEach(function(element) {
        element.get('nickname', function(err, nickname) {
            if (nickname == this.champion) {
                element.emit('victorious', nickname)
            } else {
                element.emit('loser', {
                    position: options.position,
                    type: options.type,
                    nickname: nickname
                })
            }
        }.bind(this))
    }.bind(this))
}

Game.prototype.equal = function(options) {
    this.sockets.clients(options.room).forEach(function(element) {
        element.get('nickname', function(err, nickname) {
            element.emit('equal', {
                position: options.position,
                type: options.type,
                nickname: nickname
            })
        })
    }.bind(this))
}


Game.prototype.restart = function(room) {
    var sort = [true, false],
        i = 0;

    users.push(this.user.nickname)
    if (_.uniq(users).length == 2) {
        this.sockets.clients(room).forEach(function(element) {
            if (sort[i]) {
                element.emit('init', true)
            } else {
                element.emit('init', false)
            }
            i += 1;
        })
        users = []
    }
}

module.exports = {
    played: function(sockets, redis, user, options) {
        game = new Game(sockets, redis, user);
        game.played(options)
    },

    win: function(sockets, redis, user, options) {
        game = new Game(sockets, redis, user);
        game.win(options)
    },

    equal: function(sockets, redis, user, options) {
        game = new Game(sockets, redis, user);
        game.equal(options)
    },

    restart: function(sockets, redis, user, room) {
        game = new Game(sockets, redis, user);
        game.restart(room)
    }
}