var _ = require('underscore'),
    defer = require('node-promise').defer;

function Client(sockets, redis, user) {
    this.sockets = sockets;
    this.redis = redis;
    this.user = user;
}

Client.prototype.connect = function() {
    this.sockets.clients().forEach(function(online) {
        var element = this.sockets.sockets[online.id];

        element.get('nickname', function(err, nickname) {
            if (!nickname && !online.id) {
                return
            }
            this._isPlaying(nickname).then(function(isPlaying) {
                if (!isPlaying) {
                    this.redis.hset('online', nickname, online.id, function(err, result) {
                        if (result) {
                            this._updateStatus('online')
                        }
                    }.bind(this))
                }
            }.bind(this))
        }.bind(this))
    }.bind(this));
}

Client.prototype.disconnect = function(client, UserInvitation) {
    client.get('room', function(err, roomId) {
        UserInvitation.partnerDisconnected(roomId, this.sockets, client, this.redis, this.user)
    }.bind(this))

    this._delLists(this.user.nickname)
}

Client.prototype._updateStatus = function(status) {
    this.redis.hgetall(status, function(err, result) {
        if (result) {
            values = _.values(result)
            allUsers = this._parseUserInformation(values)

            values.forEach(function(id) {
                element = this.sockets.sockets[id]
                element.get('nickname', function(err, val) {
                    users = _.omit(allUsers, val)
                })
                this._eventTriggerStatus({
                    element: element,
                    users: users,
                    values: values,
                    id: id,
                    status: status
                })
            }.bind(this))
        } else {
            return false
        }
    }.bind(this))
}

Client.prototype._parseUserInformation = function(value) {
    avatars = []
    nicknames = []

    _.each(values, function(value) {
        element = this.sockets.sockets[value]

        element.get('avatar', function(err, val) {
            avatars.push(val)
        }.bind(this))

        element.get('nickname', function(err, val) {
            nicknames.push(val)
        }.bind(this))
    }.bind(this))

    return _.object(nicknames, avatars)
}

Client.prototype._eventTriggerStatus = function(options) {
    if (options.element) {
        options.element.emit(options.status, options.users)
    } else {
        i = _.lastIndexOf(options.values, options.id)
        this._delLists(options.keys[i])
    }
}

Client.prototype._delLists = function(nickname) {
    this.redis.hdel('online', nickname, function() {
        this.redis.hdel('playing', nickname, function() {
            this._updateStatus('online')
        }.bind(this))
    }.bind(this))
}

Client.prototype._isPlaying = function(nickname) {
    var deffered = defer()

    this.redis.hexists('playing', nickname, function(err, status) {
        deffered.resolve(status == 1)
    })

    return deffered.promise
}

module.exports = {
    connect: function(sockets, redis, user) {
        init = new Client(sockets, redis, user);
        init.connect()
    },

    disconnect: function(sockets, redis, user, client, UserInvitation) {
        init = new Client(sockets, redis, user);
        init.disconnect(client, UserInvitation)
    },

    updateStatus: function(sockets, redis, user, status) {
        init = new Client(sockets, redis, user);
        init._updateStatus(status)
    }
}