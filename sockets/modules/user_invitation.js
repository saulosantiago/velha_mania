var crypto = require('crypto'),
    md5 = crypto.createHash('md5'),
    _ = require('underscore'),
    Room = require('./../modules/room');

function UserInvitation(sockets, client, redis, user) {
    if (redis) {
        this.redis = redis;
    }

    if (sockets) {
        this.sockets = sockets;
    }

    if (client) {
        this.client = client;
    }

    if (user) {
        this.user = user;
    }
}

UserInvitation.prototype.invitation = function(guestNickname, Client) {
    var roomId = this._generateRoomId(guestNickname);
    users = [this.user.nickname, guestNickname]
    this._joinRoom(roomId)
    this.redis.hmget('online', users, function(err, result) {
        if (result) {

            element = this.sockets.sockets[_.first(result)]
            guestElement = this.sockets.sockets[_.last(result)]
            guestElement.emit('receive-invitation', {
                room: roomId,
                nickname: this.user.nickname
            })
            setAll = _.flatten(_.zip(users, result))
            this.redis.hmset('playing', setAll[0], setAll[1], setAll[2], setAll[3], function(err, result) {
                if (result) {
                    element.emit('can-invite-cancel', {
                        roomId: roomId,
                        guestNickname: guestNickname
                    })
                }
            })
        }
    }.bind(this))

    users.forEach(function(element) {
        this.redis.hdel('online', element, function() {
            if (element == _.last(users)) {
                Client.updateStatus(this.sockets, this.redis, this.user, 'online')
            }
        }.bind(this))
    }.bind(this))
}

UserInvitation.prototype.inviteCancel = function(options) {
    this.redis.hget('playing', options.nickname, function(err, result) {
        element = this.sockets.sockets[result]
        this.redis.hdel('playing', options.nickname)
        if (element) {
            this._backToRoom(element)
        }
    }.bind(this))

    this.destroyedRoom(options.room)
}

UserInvitation.prototype.acceptInvite = function(room) {
    this._joinRoom(room)
    this.sockets. in (room).emit('go-to-game', room)
    Room.getNicknames(this.sockets, room)
    Room.setActionValue(this.sockets, room)
}

UserInvitation.prototype.destroyedRoom = function(room) {
    this.sockets.clients(room).forEach(function(element) {
        element.get('nickname', function(err, result) {
            this.redis.hdel('playing', result)
        }.bind(this))

        this._backToRoom(element)
    }.bind(this))

    this.client.leave(room)
}

UserInvitation.prototype.disconnect = function(room) {
    this.sockets.clients(room).forEach(function(result) {
        element = this.sockets.sockets[result.id]
        element.get('nickname', function(err, nickname) {
            if (nickname != this.user.nickname) {
                element.emit('partner-disconnected', room)
            } else {
                this._backToRoom(element)
            }
        }.bind(this))
    }.bind(this))
}

UserInvitation.prototype._generateRoomId = function(guestNickname) {
    var dateNow = new Date().toString(),
        secret = 'kjhASDJ45HBndfsadkgh128dvb',
        timestamp = secret + guestNickname + dateNow,
        md5 = crypto.createHash('md5'),
        room = md5.update(timestamp).digest('hex');

    return room
}

UserInvitation.prototype._joinRoom = function(room) {
    this.client.set('room', room)
    this.client.join(room)
}

UserInvitation.prototype._backToRoom = function(element) {
    element.emit('back-to-room')
}

module.exports = {
    invite: function(guestNickname, sockets, client, redis, user, Client) {
        userInvitation = new UserInvitation(sockets, client, redis, user);
        userInvitation.invitation(guestNickname, Client);
    },

    inviteCancel: function(sockets, client, redis, user, options) {
        userInvitation = new UserInvitation(sockets, client, redis, user, options);
        userInvitation.inviteCancel(options);
    },

    accept: function(room, sockets, client, redis) {
        userInvitation = new UserInvitation(sockets, client, redis);
        userInvitation.acceptInvite(room);
    },

    destroy: function(room, sockets, client, redis) {
        userInvitation = new UserInvitation(sockets, client, redis, null);
        userInvitation.destroyedRoom(room);
    },

    partnerDisconnected: function(room, sockets, client, redis, user) {
        userInvitation = new UserInvitation(sockets, client, redis, user)
        userInvitation.disconnect(room);
    }
}