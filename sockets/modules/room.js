var defer = require('node-promise').defer,
    _ = require('underscore');

function Room(sockets) {
    this.sockets = sockets
}

Room.prototype.getNicknames = function(room) {
    this._getUsers(room).then(function(result) {
        this.sockets.clients(room).forEach(function(client) {
            var element = this.sockets.sockets[client.id];
            element.get('nickname', function(err, nickname) {
                opponent = _.omit(result, nickname)
                itsMe = _.omit(result, _.first(_.keys(opponent)))
                element.emit('set-nicknames', {
                    itsMe: itsMe,
                    opponent: opponent
                })
            })
        }.bind(this))
    }.bind(this))
}

Room.prototype.setActionValue = function(room) {
    var i = 0,
        type = _.shuffle(['X', 'O']),
        unlocked = _.shuffle([true, false]);

    this.sockets.clients(room).forEach(function(element) {
        element.emit('set-action-value', {
            type: type[i],
            unlocked: unlocked[i]
        })
        i += 1;
    })
}

Room.prototype._getUsers = function(room) {
    var nicknames = [],
        avatars = [],
        deferred = defer();

    this.sockets.clients(room).forEach(function(player) {
        var element = this.sockets.sockets[player.id];
        element.get('nickname', function(err, result) {
            nicknames.push(result)
        });
        element.get('avatar', function(err, result) {
            avatars.push(result)
        });
    }.bind(this))

    deferred.resolve(_.object(nicknames, avatars))
    return deferred.promise
}

module.exports = {
    getNicknames: function(sockets, roomId) {
        room = new Room(sockets);
        room.getNicknames(roomId);
    },

    setActionValue: function(sockets, roomId) {
        room = new Room(sockets);
        room.setActionValue(roomId);
    }
}