var defer = require('node-promise').defer

function UsersCount(sockets, redis) {
    this.redis = redis
    this.sockets = sockets
}

UsersCount.prototype.init = function(type) {
    switch (type) {
        case 'playing':
            this.playing()
            break;

        case 'online':
            this.onlines()
            break;

        default:
            this.all()
    }
}

UsersCount.prototype._onlines = function() {
    var deferred = defer()

    this.redis.hlen('online', function(err, result) {
        deferred.resolve(result)
    })

    return deferred.promise
}

UsersCount.prototype._playing = function() {
    var deferred = defer()

    this.redis.hlen('playing', function(err, result) {
        deferred.resolve(result)
    })

    return deferred.promise
}

UsersCount.prototype._all = function() {
    var deferred = defer(),
        result = 0;

    this._onlines()
        .then(function(onlines) {
            result = onlines
            this._playing()
                .then(function(playing) {
                    deferred.resolve(result += playing)
                })
        }.bind(this))

    return deferred.promise
}

UsersCount.prototype.all = function() {
    this._all().then(function(result) {
        this.sockets.emit('return-all-users-count', result)
    }.bind(this))
}

UsersCount.prototype.playing = function() {
    this._playing().then(function(result) {
        this.sockets.emit('return-rooms-count', Math.ceil(result / 2))
    }.bind(this))
}

UsersCount.prototype.onlines = function() {
    this._playing().then(function(result) {
        this.sockets.emit('return-onlines-users-count', result)
    }.bind(this))
}

module.exports = {
    get: function(type, sockets, redis) {
        usersCount = new UsersCount(sockets, redis);
        usersCount.init(type);
    }
}