module.exports = function(io) {
    var redisConnect = require('../middleware/redis_connect'),
        redis = redisConnect.getClient(),
        sockets = io.sockets,
        Client = require('./modules/client'),
        UsersCount = require('./modules/users_count'),
        UserInvitation = require('./modules/user_invitation'),
        Room = require('./modules/room'),
        Game = require('./modules/game');

    sockets.on('connection', function(client) {
        var session = client.handshake.session,
            user = session.user;

        if (!user) {
            return
        }
        client.set('nickname', user.nickname);
        client.set('avatar', user.avatar);
        Client.connect(sockets, redis, user)

        client.on('invite', function(guestNickname) {
            UserInvitation.invite(guestNickname, sockets, client, redis, user, Client)
        })

        client.on('invite-cancel', function(options) {
            UserInvitation.inviteCancel(sockets, client, redis, user, options)
        })

        client.on('accept-invitation', function(room) {
            UserInvitation.accept(room, sockets, client, redis)
        })

        client.on('rejected-invitation', function(room) {
            UserInvitation.inviteCancel(sockets, client, redis, user, {
                room: room,
                nickname: user.nickname
            })
        })

        client.on('user-left', function(room) {
            UserInvitation.partnerDisconnected(room, sockets, client, redis, user)
        })

        client.on('destroyed-room', function(room) {
            UserInvitation.destroy(room, sockets, client, redis)
        })

        client.on('get-all-users', function() {
            UsersCount.get(null, sockets, redis)
        })

        client.on('get-rooms-count', function() {
            UsersCount.get('playing', sockets, redis)
        })

        client.on('played', function(options) {
            Game.played(sockets, redis, user, options)
        })

        client.on('win', function(options) {
            Game.win(sockets, redis, user, options)
        })

        client.on('equal', function(options) {
            Game.equal(sockets, redis, user, options)
        })

        client.on('restart-game', function(room) {
            Game.restart(sockets, redis, user, room)
        })

        client.on('disconnect', function() {
            Client.disconnect(sockets, redis, user, client, UserInvitation)
        })
    });
}