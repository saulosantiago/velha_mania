Module("VelhaMania.Locales", function(Locales) {
    Locales.fn.initialize = function() {}

    Locales.fn.get = function() {
        if (location.pathname.match(/en/)) {
            return Locale.en
        } else {
            return Locale.ptBR
        }
    }

    Locales.fn.i18n = function() {
        divs = $('div[data-translate]')

        $.each(divs, function(i, element) {
            result = $(element).data('translate');
            result = result.split('.')
            namespace = _.first(result)
            key = _.last(result)
            value = this.get()[namespace][key]

            $(element)
                .text(value)
                .contents().unwrap()

        }.bind(this))
    }
})