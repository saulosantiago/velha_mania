Module("VelhaMania.Timer", function(Timer) {
    Timer.fn.initialize = function() {
        this.timer = 30000;
        this.interval = 1000;

        this.out = undefined;
        this.intAsc = undefined;
        this.intDesc = undefined;

        Emitter.extend(this);
    };

    Timer.fn.init = function(options) {
        this._reset()

        if (options.timer) {
            this.timer = options.timer
        }
        if (options.interval) {
            this.interval = options.interval
        }

        switch (options.type) {
            case 'timeout':
                this.timeOut()
                break;
            case 'interval:asc':
                this.asc()
                break;
            case 'interval:desc':
                this.desc()
                break;
        }
    }

    Timer.fn.timeOut = function() {
        this.out = setTimeout(function() {
            this.emit('done')
            this._reset()
        }.bind(this), this.timer)
    }

    Timer.fn.asc = function() {
        this.timeOut()
        var count = 0;

        this.intAsc = setInterval(function() {
            this.emit('hit:asc', count += 1)
        }.bind(this), this.interval)
    }

    Timer.fn.desc = function() {
        this.timeOut()
        var count = +this.timer * 0.001;

        this.intAsc = setInterval(function() {
            this.emit('hit:desc', count -= 1)
        }.bind(this), this.interval)
    }

    Timer.fn._reset = function() {
        clearTimeout(this.out)
        clearInterval(this.intDesc)
        clearInterval(this.intAsc)
        this.off('hit:desc')
        this.off('hit:asc')
        this.off('done')
    }

    Timer.fn.stop = function() {
        this._reset()
    }
});