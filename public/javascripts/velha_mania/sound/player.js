Module("VelhaMania.Player", function(Player) {
    Player.fn.initialize = function(tracks) {
        this.tracks = tracks
        this.playing = undefined
        this.ended = []
        this.container = $('#player')
        this.increaseVolume = this.container.find('.increase_volume')
        this.decreaseVolume = this.container.find('.decrease_volume')
        this.sort()
        this.init()
        this.addEventListeners()
    };

    Player.fn.trackInstance = function(track) {
        if (!_.isString(track)) return track
        return new buzz.sound('/sounds/' + track, this.options())
    }

    Player.fn.options = function() {
        return {
            formats: ["ogg", "mp3", "aac", "wav"],
            preload: true,
            autoplay: false,
            loop: false
        }
    }

    Player.fn.sort = function() {
        this.tracks = _.shuffle(this.tracks)
    }

    Player.fn.buttons = function() {
        this.playButton = this.container.find('.play')
        this.pauseButton = this.container.find('.pause')
        this.nextButton = this.container.find('.next')
        this.prevButton = this.container.find('.prev')
        this.stopButton = this.container.find('.stop')
    }

    Player.fn.init = function() {
        if (this.playing) {
            this.stop()
            return
        }
        this.playerRender()
        this.togglePlayPause()
        this.setPlayingTrack()
    }

    Player.fn.setPlayingTrack = function(track) {
        if (_.isEmpty(this.tracks) && !_.isEmpty(this.ended)) {
            this.tracks = this.ended
            this.ended = []
        }

        if (track) {
            currentTrack = track
        } else {
            currentTrack = this.tracks.shift()
        }

        this.ended.splice(0, 0, currentTrack)
        track = this.trackInstance(currentTrack)
        this.playing = track
        this.play()
        this.currentTrackBind()
    }

    Player.fn.currentTrackBind = function() {
        this.playing.bind('ended', function() {
            this.playing = undefined
            this.setPlayingTrack()
        }.bind(this))
    }

    Player.fn.addEventListeners = function() {
        this.container.on('click', '.play', function() {
            this.playReturn()
        }.bind(this))

        this.container.on('click', '.pause', function() {
            this.pause()
        }.bind(this))

        this.container.on('click', '.next', function() {
            this.next()
        }.bind(this))

        this.container.on('click', '.prev', function() {
            this.prev()
        }.bind(this))

        this.container.on('click', '.mute', function() {
            if (!this.playing) {
                return
            }
            this.playing.toggleMute()
        }.bind(this))

        this.container.on('click', '.increase_volume', function() {
            if (!this.playing) {
                return
            }
            this.playing.increaseVolume(10)
        }.bind(this))

        this.container.on('click', '.decrease_volume', function() {
            if (!this.playing) {
                return
            }
            this.playing.decreaseVolume(10)
        }.bind(this))
    }

    Player.fn.play = function() {
        this.playing.play().fadeIn()
        this.setTimer()
    }

    Player.fn.pause = function() {
        this.playing.pause()
        this.togglePlayPause()
    }

    Player.fn.stop = function() {
        this.playing.unbind('timeupdate')
        this.playing.stop()
        this.tracks.push(this.ended)

        this.playing = undefined
        this.ended = []

        this.tracks = _.flatten(this.tracks)
        this.togglePlayPause()
    }

    Player.fn.prev = function() {
        if (this.ended.length > 1) {
            this.playing.stop()
            this.playing = undefined
            this.tracks.push(this.ended.pop())
            this.tracks.push(this.ended.pop())
            this.setPlayingTrack()
        }
    }

    Player.fn.next = function() {
        if (!_.isEmpty(this.tracks)) {
            this.playing.stop()
            this.setPlayingTrack()
        }
    }

    Player.fn.playReturn = function() {
        this.togglePlayPause()
        this.play()
    }

    Player.fn.togglePlayPause = function() {
        this.pauseButton.toggle()
        this.playButton.toggle()
    }

    Player.fn.playerRender = function() {
        this.buttons()
        player = this.container.find('.content')

        if (player.length == 0) {
            html = VelhaMania.templates.player()
            this.container.html(html)
        }
    }

    Player.fn.close = function() {
        $('#player').html('')
    }

    Player.fn.setTimer = function() {
        _self = this
        this.playing.bind('timeupdate', function(a) {
            currentTime = buzz.toTimer(this.getTime())
            $('.current_time').text(currentTime)
            _self.progressBar()
        })

        this.playing.bind('canplay', function() {
            total = buzz.toTimer(this.playing.getDuration())
            $('.total_time').text(total)
        }.bind(this))
    }

    Player.fn.progressBar = function() {
        progressBar = $('.progress_bar')
        width = progressBar.width()
        progress = (width * this.playing.getPercent()) / 100
        progressBar.find('.slider').css({
            width: progress
        })
    }
});