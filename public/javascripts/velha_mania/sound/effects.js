Module("VelhaMania.Effects", function(Effects) {
    Effects.fn.initialize = function() {
        path = '/sounds/effects/'
        this.clickSound = path + 'click'
        this.receivedInvitationSound = path + 'received_invitation'
        this.loggedRoomSound = path + 'logged_room'
        this.winSound = path + 'win'
        this.loseSound = path + 'lose'
        this.moveSound = path + 'move'
        this.myTurnSound = path + 'my_turn'
        this.currentPlaying = undefined
    }

    Effects.fn.click = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.clickSound, this.options()).play()
    }

    Effects.fn.receivedInvitation = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.receivedInvitationSound, this.options()).play()
    }

    Effects.fn.loggedRoom = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.loggedRoomSound, this.options()).play()
    }

    Effects.fn.win = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.winSound, this.options()).play()
    }

    Effects.fn.lose = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.loseSound, this.options()).play()
    }

    Effects.fn.move = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.moveSound, this.options()).play()
    }

    Effects.fn.myTurn = function() {
        if (!_.isEmpty(this.currentPlaying)) {
            this.currentPlaying.stop()
        }
        this.currentPlaying = new buzz.sound(this.myTurnSound, this.options()).play()
    }

    Effects.fn.options = function() {
        return {
            formats: ["ogg", "mp3", "aac", "wav"],
            preload: true,
            autoplay: false,
            setVolume: 40,
            loop: false
        }
    }
})