Module("VelhaMania.Room", function(Room) {
    Room.fn.initialize = function() {
        this.render()
        this.addEventListeners()
        this.UserList = VelhaMania.UsersList(this)
    }

    Room.fn.addEventListeners = function() {
        socket.on('online', function(options) {
            this.UserList.listUpdate(options)
            socket.emit('get-all-users')
        }.bind(this));

        socket.on('offline', function(nicknames) {
            this.UserList.listUpdate(nicknames)
        }.bind(this));
    }

    Room.fn.render = function() {
        html = VelhaMania.templates.room()
        $('#room').html(html)
    }

    Room.fn.close = function() {
        $('#room').html('')
    }
});