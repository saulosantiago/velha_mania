Module("VelhaMania.CountAllUsers", function(CountAllUsers) {
    CountAllUsers.fn.initialize = function() {
        this.addEventListeners()
    };

    CountAllUsers.fn.addEventListeners = function() {
        socket.on('return-all-users-count', function(total) {
            VelhaMania.CountMatches()
            $('h3 span').text(total)
        });
    };
})