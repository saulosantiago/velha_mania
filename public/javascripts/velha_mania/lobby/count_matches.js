Module("VelhaMania.CountMatches", function(CountMatches) {
    CountMatches.fn.initialize = function() {
        this.addEventListeners()
        socket.emit('get-rooms-count')
    };

    CountMatches.fn.addEventListeners = function() {
        socket.on('return-rooms-count', function(total) {
            $('#matches b span').text(total + ' ')
        });
    };
});