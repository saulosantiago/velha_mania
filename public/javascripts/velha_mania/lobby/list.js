Module("VelhaMania.UsersList", function(UsersList) {
    UsersList.fn.initialize = function(room) {
        this.room = room
        this.addEventListeners()
        this.userClickedList()
        VelhaMania.CountAllUsers()
        sendAnalitycs({
            1: 'room'
        })
    };

    UsersList.fn.addEventListeners = function() {
        socket.on('receive-invitation', function(options) {
            effects.receivedInvitation()
            var message = options.nickname + ' ' + i18n.room.matchInvite;
            modal = VelhaMania.Modal()
            $('body').append(modal.confirmation(message))

            modal
                .on('accept', function() {
                    socket.emit('accept-invitation', options.room)
                })
                .on('reject', function() {
                    socket.emit('rejected-invitation', options.room)
                })
        }.bind(this));

        socket.on('back-to-room', function() {
            location.reload()
        });

        socket.on('go-to-game', function(roomId) {
            this.room.close()
            html = VelhaMania.templates.game()
            $('#display_game').html(html)
            VelhaMania.Player(['game/track_1', 'game/track_2', 'game/track_3', 'game/track_4', 'game/track_5'])
            VelhaMania.Table(roomId)
            effects.loggedRoom()
        }.bind(this));

        socket.on('can-invite-cancel', function(options) {
            this.cancelInvite(options)
        }.bind(this))
    };

    UsersList.fn.userClickedList = function() {
        $('#users ul').on('click', 'li', function(e) {
            e.preventDefault()
            effects.click()
            nickname = $(this).data('nickname')
            $(this).addClass('wait')
            socket.emit('invite', nickname)
            $('#users ul').off('click', 'li')
        });
    };

    UsersList.fn.cancelInvite = function(options) {

        var cancelButton = $('<a>', {
            class: 'invite_cancel'
        }).text(' ' + i18n.room.cancelInvite).prepend($('<span>', {
            class: 'timer'
        }))

        $('#users ul li.wait').append(cancelButton)

        timer.init({
            type: 'interval:desc',
            timer: 31000
        })

        timer.on('hit:desc', function(hit) {
            $('span.timer').text(hit)
        })

        timer.on('done', function() {
            socket.emit('invite-cancel', {
                room: options.roomId,
                nickname: options.guestNickname
            })
        })

        $('#users ul li').on('click', 'a.invite_cancel', function(e) {
            e.preventDefault()
            socket.emit('invite-cancel', {
                room: options.roomId,
                nickname: options.guestNickname
            })
        })
    }

    UsersList.fn.listUpdate = function(users) {
        if (!users) {
            return
        }
        lis = $('li')
        names = _.map(lis, function(element) {
            return $(element).text()
        })
        nicknames = _.keys(users)
        avatars = _.values(users)
        differences = _.difference(names, nicknames)

        $.each(differences, function(index, nickname) {
            $("li:contains('" + nickname + "')").remove()
        })

        $.each(nicknames, function(index, nickname) {
            if (!_.contains(names, nickname)) {

                li = $('<li>', {
                    "data-nickname": nickname
                }).text(nickname.capitalize()).prepend($('<img>', {
                    src: avatars[index],
                    width: 60,
                    height: 60
                }))

                $('#users ul').prepend(li)
            }
        })
    };
});