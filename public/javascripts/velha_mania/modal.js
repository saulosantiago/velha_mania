Module("VelhaMania.Modal", function(Modal) {
    Modal.fn.initialize = function() {
        Emitter.extend(this)
        this.modalConfimation = $('#modal.confirmation')
        this.modalAlert = $('#modal.alert')
    };

    Modal.fn.confirmation = function(message) {
        this.removeAll()
        return VelhaMania.templates.modal_confirmation({
            message: message
        })
    }

    Modal.fn.alert = function(message) {
        this.removeAll()
        return VelhaMania.templates.modal_alert({
            message: message
        })
    }

    Modal.fn.accepted = function() {
        this.emit('accept')
        this.modalConfimation.remove()
    }

    Modal.fn.rejected = function() {
        this.emit('reject')
        this.modalConfimation.fadeOut("slow").remove()
    }

    Modal.fn.ok = function() {
        this.emit('ok')
        this.modalAlert.remove()
    }

    Modal.fn.removeAll = function() {
        this.modalConfimation.remove()
        this.modalAlert.remove()
    }
});