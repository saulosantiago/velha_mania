(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
    m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-49972840-1', 'velhamania.com.br');
ga('send', 'pageview');

window.analyticsDimensions = function(dimensions) {
    customDimensions = {};
    $.each(dimensions, function(key, value) {
        customDimensions["dimension" + key] = value;
    });
    return customDimensions;
}

sendAnalitycs = function(dimensions) {
    ga('send', 'pageview', 'sala', dimensions)
    analyticsDimensions(dimensions)
}