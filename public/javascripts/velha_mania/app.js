Module("VelhaMania.App", function(App) {
    App.fn.initialize = function() {
        this.locale()
        this.timer()
        this.effects()
    };

    App.fn.locale = function() {
        l = VelhaMania.Locales();
        l.i18n()
        i18n = l.get()
    }

    App.fn.timer = function() {
        timer = VelhaMania.Timer()
    }

    App.fn.effects = function() {
        effects = VelhaMania.Effects()
    }
});