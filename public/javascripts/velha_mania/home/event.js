window.Events = (function() {
    Events.name = 'Events';

    function Events() {}

    Events.afterLogin = function() {
        $(this).trigger('login');
    };

    return Events;
}).call(this);