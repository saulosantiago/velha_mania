Module("VelhaMania.FbCallback", function(FbCallback) {
    FbCallback.fn.initialize = function() {
        if (window.location.pathname.match(/callback\/facebook/)) {
            this.setInputQueries()
            this.addEventListeners()
            $("input[name='user[birthdate]'").mask("9?9/99/9999")
        }
    }

    FbCallback.fn.addEventListeners = function() {
        var _self = this
        $('form').submit(function(e) {
            e.preventDefault()
            form = $(this)

            if (_self.validate()) {
                var response = $.ajax({
                    dataType: 'json',
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize()
                })

                response.success(function(data) {
                    _self.loginRequester(data)
                })
            } else {
                return false
            }
        })
    }

    FbCallback.fn.loginRequester = function(data) {
        var login = VelhaMania.FbAuthentication();
        login.execute(data.result)

        login.on('fbLogged', function(data) {
            if (data.status && data.message == 'success') {
                this.afterLogin()
            }
        }.bind(this))
    }

    FbCallback.fn.afterLogin = function() {
        window.opener.Events.afterLogin()
        window.close()
    }

    FbCallback.fn.validate = function() {
        var status = true,
            _self = this;

        dateInput = $("input[name='user[birthdate]']")
        emailInput = $("input[name='user[email]']")

        switch (true) {
            case emailInput.val() == '':
                _self.validFields()
                _self.invalidField(emailInput, i18n.facebook['fieldRequired'])
                status = false
                break;
            case !_self.isValidEmail(emailInput.val()):
                _self.validFields()
                _self.invalidField(emailInput, i18n.facebook['invalidEmail'])
                status = false
                break;
            case dateInput.val() == '':
                _self.validFields()
                _self.invalidField(dateInput, i18n.facebook['fieldRequired'])
                status = false
                break;
            case !_self.isValidDate(dateInput.val()):
                _self.validFields()
                _self.invalidField(dateInput, i18n.facebook['invalidDate'])
                status = false
                break;
            default:
                _self.validFields()
                status = true
        }

        return status
    }

    FbCallback.fn.invalidField = function(input, message) {
        $(input)
            .focus()
            .addClass('invalid')
            .next().show()
            .text(message)
    }

    FbCallback.fn.validFields = function() {
        $('input:visible')
            .removeClass('invalid')
            .next().hide()
    }

    FbCallback.fn.isValidEmail = function(value) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(value)
    }

    FbCallback.fn.isValidDate = function(value, userFormat) {
        var userFormat = userFormat || 'dd/mm/yyyy',
            delimiter = /[^dmy]/.exec(userFormat)[0],
            theFormat = userFormat.split(delimiter),
            theDate = value.split(delimiter);

        isDate = function(date, format) {
            var d, m, y
            for (var i = 0, len = format.length; i < len; i++) {
                if (/d/.test(format[i])) d = date[i]
                if (/m/.test(format[i])) m = date[i]
                if (/y/.test(format[i])) y = date[i]
            }

            return (
                d > 0 && d <= (new Date(y, m, 0)).getDate() &&
                m > 0 && m < 13 &&
                y && y.length === 4
            )
        }

        return isDate(theDate, theFormat)
    }

    FbCallback.fn.setInputQueries = function() {
        var hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            if (hash[0].match(/providers/)) {
                hash[0] = hash[0].replace(/\_/, '][')
                hash[1] = hash[1].replace('#_', '')
            }

            $("input[name='user[" + hash[0] + "]']").val(unescape(hash[1]))
        }
    }
})