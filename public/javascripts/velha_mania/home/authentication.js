Module("VelhaMania.Login", function(Login) {
    Login.fn.initialize = function() {
        this.addEventListeners()
        this.buttonClicked()
    },

    Login.fn.buttonClicked = function() {
        $('a.cj').click(function() {
            this.openPopup({
                url: 'http://connect.clickjogos.uol.com.br/' + this.cjKey(),
                width: 630,
                height: 636
            })
        }.bind(this))

        $('a.facebook').click(function() {
            this.openPopup({
                url: '/auth/facebook/',
                width: 990,
                height: 630
            })
        }.bind(this))
    },

    Login.fn.cjKey = function() {
        var key = undefined

        if (location.host == 'velhamania.com.br') {
            key = '60895cfa56db8cca533e11122563bbe2'
        } else {
            key = 'fe96c32683ccae7254ebf9caf3afa60f'
        }

        return key
    }

    Login.fn.openPopup = function(options) {
        var left = (screen.width / 2) - (options.width / 2),
            top = (screen.height / 2) - (options.height / 2);

        open(options.url, 'sharer', 'toolbar=no, width=' + options.width + ', height=' + options.height + ', top=' + top + ', left=' + left)
    },

    Login.fn.addEventListeners = function() {
        $(Event).on('login', function() {
            location.href = '/sala' + '/' + locale()
        }.bind(this))
    }
});