Module("VelhaMania.CjAuthentication", function(CjAuthentication) {
    CjAuthentication.fn.initialize = function() {
        Emitter.extend(this)
    }

    CjAuthentication.fn.execute = function(obj) {
        var response = $.ajax({
            url: "/cj/authentication/",
            type: "POST",
            data: {
                user: obj
            },
        });

        response.success(function(data) {
            this.emit('cjLogged', data)
        }.bind(this))
    }
});