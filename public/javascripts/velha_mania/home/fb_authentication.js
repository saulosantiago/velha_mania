Module("VelhaMania.FbAuthentication", function(FbAuthentication) {
    FbAuthentication.fn.initialize = function() {
        Emitter.extend(this)
    }

    FbAuthentication.fn.execute = function(obj) {
        var response = $.ajax({
            url: "/facebook/authentication/",
            type: "POST",
            data: {
                user: obj
            },
        });

        response.success(function(data) {
            this.emit('fbLogged', data)
        }.bind(this))
    }
});