Module("VelhaMania.CjCallback", function(CjCallback) {
    CjCallback.fn.initialize = function() {
        if (window.location.pathname.match(/callback\/cj/)) {
            this.scope = location.search.substring(1).split('&')[0].split('=')[1]
            this.requester()
        }
    }

    CjCallback.fn.requester = function() {
        var promise = this.ssoConnect()

        promise.done(function(userInfo) {
            var user = this.userSave(userInfo);
            user.done(function(data) {
                this.requesterLogin(data)
            }.bind(this))
        }.bind(this))
    }

    CjCallback.fn.requesterLogin = function(data) {
        var login = VelhaMania.CjAuthentication();

        if (data.status && data.message == 'success') {
            login.execute(data.result)

            login.on('cjLogged', function(data) {
                if (data.status && data.message == 'success') {
                    this.afterLogin()
                }
            }.bind(this))
        }
    }

    CjCallback.fn.ssoConnect = function() {
        var deferred = $.Deferred();

        var response = $.ajax({
            url: this.ssoPath() + this.getUrlVar('uid'),
            type: 'GET',
            dataType: 'jsonp'
        });

        response.success(function(data) {
            deferred.resolve(data)
        })

        return deferred.promise()
    }

    CjCallback.fn.ssoPath = function() {
        var key = undefined;
        if (this.scope == 'production') {
            key = 'http://connect.clickjogos.uol.com.br/user/60895cfa56db8cca533e11122563bbe2/b2d8c027b08ef1d0c6006a7164f1b7b3/'
        } else {
            key = 'http://connect.clickjogos.uol.com.br/user/fe96c32683ccae7254ebf9caf3afa60f/cc09737d95bc35a0783cab922f6dac2b/'
        }

        return key
    }

    CjCallback.fn.getUrlVars = function() {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0])
            vars[hash[0]] = hash[1];
        }
        return vars
    }

    CjCallback.fn.getUrlVar = function(name) {
        return this.getUrlVars()[name]
    }

    CjCallback.fn.userSave = function(userInfo) {
        var deferred = $.Deferred(),
            obj = this.parseObj(userInfo);

        var response = $.ajax({
            url: "/user/save/",
            type: "POST",
            data: {
                user: obj
            },
        });

        response.success(function(data) {
            deferred.resolve(data)
        })

        return deferred.promise()
    }


    CjCallback.fn.parseObj = function(user) {
        return {
            nickname: user.info.nickname,
            email: user.info.email,
            gender: user.info.gender,
            birthdate: user.info.birthdate,
            avatar: this.imageUrl(user.info.avatar_thumb),
            confirmatedAt: null,
            disputes: [],
            providers: {
                name: user.provider,
                uid: user.uid
            }
        }
    }

    CjCallback.fn.imageUrl = function(img) {
        if (img.match(/asset/)) {
            img = 'http://clickjogos.uol.com.br' + img
        }

        return img
    }

    CjCallback.fn.afterLogin = function() {
        window.opener.Events.afterLogin()
        window.close()
    }
})