window.locale = function() {
    if (location.pathname.match(/en/)) {
        return 'en'
    } else {
        return ''
    }
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1)
}

window.screenTop = function() {
    $(window)
        .scrollTop(0)
        .scrollLeft(0)
}

window.scrollLock = function() {
    $('body').css('overflow', 'hidden')
}

window.scrollUnlock = function() {
    $('body').css('overflow', 'auto')
}