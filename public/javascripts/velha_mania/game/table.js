Module("VelhaMania.Table", function(Table) {
    Table.fn.initialize = function(room) {
        this.room = room;
        this.addEventListeners()
        this.setPoints = VelhaMania.SetPoints()
        this.clickOut()
        sendAnalitycs({
            1: 'play'
        })
    };

    Table.fn.addEventListeners = function() {
        socket.on('set-nicknames', function(options) {
            this.setNicknames(options)
            this.setPoints.stats(options)
        }.bind(this))

        socket.on('partner-disconnected', function(room) {
            this.autoRedirectRoom(room)
        }.bind(this))

        socket.on('set-action-value', function(options) {
            this.setValue(options.type)
            VelhaMania.Rules(this.room, options.unlocked, options.type)
        }.bind(this))
    };

    Table.fn.autoRedirectRoom = function(room) {
        modal = VelhaMania.Modal()
        $('body').append(modal.alert(i18n.game.userDisconnected))

        modal.on('ok', function() {
            socket.emit('destroyed-room', room)
        })
    }

    Table.fn.setNicknames = function(nicknames) {
        var points = $('#points')
        points.find('.player_one img').attr({
            src: _.values(nicknames.itsMe)
        })
        points.find('.player_two img').attr({
            src: _.values(nicknames.opponent)
        })
        points.find('.player_one span.name').text(_.keys(nicknames.itsMe))
        points.find('.player_two span.name').text(_.keys(nicknames.opponent))
    }

    Table.fn.setValue = function(value) {
        var displayGame = $('#display_game')
        displayGame.find('#points .action').attr('data-action', value)
        displayGame.find('#information span').text(' ' + value)
    }

    Table.fn.clickOut = function() {
        modal = VelhaMania.Modal();

        $('#display_game').on('click', '.exit', function(e) {
            e.preventDefault()
            $('body').append(modal.confirmation(i18n.game.exitConfirmation))

            modal.on('accept', function() {
                socket.emit('user-left', this.room)
            }.bind(this))
        }.bind(this))
    }
})