Module("VelhaMania.Report", function(Report) {
    Report.fn.initialize = function() {};

    Report.fn.detailsSave = function(status) {

        var deferred = $.Deferred(),
            data = this.parseMatchesInformation(status);

        var response = $.ajax({
            url: "/save-game-details/",
            type: "POST",
            data: data,
        });

        response.success(function(data) {
            deferred.resolve(data)
        })

        return deferred.promise()
    }

    Report.fn.getNicknames = function() {
        result = {
            itsMe: $(_.first($('span.name'))).text(),
            opponent: $(_.last($('span.name'))).text()
        }

        return result
    }

    Report.fn.parseMatchesInformation = function(status) {
        var result = undefined

        switch (status) {
            case 'win':
                result = {
                    win: 1,
                    draw: 0,
                    lose: 0
                };
                break;
            case 'loser':
                result = {
                    win: 0,
                    draw: 0,
                    lose: 1
                };
                break;
            default:
                result = {
                    win: 0,
                    draw: 1,
                    lose: 0
                };
        }

        data = {
            itsMe: this.getNicknames().itsMe,
            win: result.win,
            draw: result.draw,
            lose: result.lose,
            matches: 1,
            opponent: this.getNicknames().opponent
        }

        return {
            data: data
        }
    }
})