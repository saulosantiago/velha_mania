Module("VelhaMania.Rules", function(Rules) {
    Rules.fn.initialize = function(room, unlocked, type) {
        l.i18n()
        this.room = room;
        this.message = $('.message');
        this.board = $('#board');
        this.type = type;
        this.unlocked = unlocked;
        this.end = false;
        this.confirmationText = ''
        this.addEventListeners();
        this.report = VelhaMania.Report();
        this.setPoints = VelhaMania.SetPoints()
        this.turnOrientation()
        this.adjustSize()
    };

    Rules.fn.adjustSize = function() {
        var overlay = $('#board .overlay'),
            information = $('#information');

        $('header').fadeOut('slow', function() {
            $('html, body').animate({
                scrollTop: information.offset().top
            }, 800)
        })

        var screenHeight = $(window).height() - information.height()
        overlay
            .width(screenHeight)
            .height(screenHeight)
    }

    Rules.fn.addEventListeners = function() {
        this.board.on('click', 'div', function(e) {
            effects.move()
            element = $(e.currentTarget)
            this.played(element)
        }.bind(this))

        socket.on('unlocked', function(options) {
            this.unlocked = options.status;
            element = $('.' + options.position);
            this.setPosition(element, options.type)
            this.youTurn()
        }.bind(this))

        socket.on('locked', function(status) {
            this.unlocked = status;
            this.notYouTurn()
        }.bind(this))

        socket.on('victorious', function(nickname) {
            effects.win()
            this.stop()
            this.win()
            this.report.detailsSave('win')
            this.setPoints.update(nickname, {
                win: 1
            })
            this.restart()
        }.bind(this))

        socket.on('loser', function(options) {
            effects.lose()
            this.stop()
            this.loser()
            this.report.detailsSave('loser')
            element = $('.' + options.position);
            this.setPosition(element, options.type)
            this.setPoints.update(options.nickname, {
                lose: 1
            })
            this.restart()
        }.bind(this))

        socket.on('equal', function(options) {
            this.stop()
            this.equal()
            this.report.detailsSave('equal')
            element = $('.' + options.position);
            this.setPosition(element, options.type)
            this.setPoints.update(options.nickname, {
                draw: 1
            })

            this.restart()
        }.bind(this))

        socket.on('init', function(status) {
            this.adjustSize()
            this.unlocked = status;
            this.end = false;
            this.turnOrientation()
        }.bind(this))
    }

    Rules.fn.turnOrientation = function() {
        if (this.unlocked) {
            this.youTurn()
        } else {
            this.notYouTurn()
        }
    }

    Rules.fn.played = function(element) {
        if (this.unlocked && this.isEmpty(element)) {
            position = $(element).attr('class');

            type = this.type;
            this.setPosition(element, type)
            emptyCountMarkings = +this.board.find("div[data-type='undefined']").length

            var promise = this.iWon(type);
            promise.done(function(status) {
                params = {
                    room: this.room,
                    position: position,
                    type: type
                }

                if (status) {
                    socket.emit('win', params)
                } else {
                    if (emptyCountMarkings == 1) {
                        socket.emit('equal', params)
                    } else {
                        socket.emit('played', params)
                    }
                }
            }.bind(this))
        }
    }

    Rules.fn.youTurn = function() {
        effects.myTurn()
        this.setTimer()
        if (this.unlocked) {
            this.setMessage({
                message: i18n.game.youTurn,
                klass: 'positive'
            })
        }
    }

    Rules.fn.notYouTurn = function() {
        this.setTimer()
        if (this.end) {
            this.setMessage({
                message: i18n.game.inviteWait,
                klass: 'negative'
            })
        } else if (!this.unlocked) {
            this.setMessage({
                message: i18n.game.notYouTurn,
                klass: 'negative'
            })
        }
    }

    Rules.fn.setTimer = function() {
        timer.init({
            type: 'interval:asc'
        })
        timer.on('hit:asc', function(hit) {
            hit += 2
            var selector = $('#progress_radial'),
                klass = 'progress_' + (hit * Math.floor(100 / 30));

            selector.removeClass()
            selector.addClass(klass)
            selector.find('.overlay').text(hit - 1)
        })

        if (this.unlocked) {
            timer.on('done', function() {
                this.automaticSort()
            }.bind(this))
        }
    }

    Rules.fn.automaticSort = function() {
        element = this.board.find("div[data-type='undefined']").first()
        this.played($(element))
    }

    Rules.fn.win = function() {
        screenTop()
        this.confirmationText += i18n.game.youWon + '\n';
        this.setMessage({
            message: i18n.game.youWon,
            klass: 'win'
        })
    }

    Rules.fn.loser = function() {
        screenTop()
        this.confirmationText += i18n.game.youLost + '\n';
        this.setMessage({
            message: i18n.game.youLost,
            klass: 'loser'
        })
    }

    Rules.fn.equal = function() {
        screenTop()
        this.confirmationText += i18n.game.youDrew + '\n';
        this.setMessage({
            message: i18n.game.youDrew,
            klass: 'equal'
        })
    }

    Rules.fn.setMessage = function(options) {
        this.clearMessages()
        this.message
            .addClass(options.klass)
            .html(options.message)
    }

    Rules.fn.clearMessages = function() {
        this.message.html('')
        this.message.removeClass('negative positive win')
    }

    Rules.fn.stop = function() {
        this.unlocked = false;
        this.end = true;
        timer.stop()
    }

    Rules.fn.clearBoard = function() {
        markings = this.board.find("div[data-type]")

        $.each(markings, function(i, element) {
            $(element).html('')
            $(element).attr('data-type', 'undefined')
        })
    }

    Rules.fn.restart = function() {
        modal = VelhaMania.Modal()
        this.confirmationText += i18n.game.playAgain
        $('body').append(modal.confirmation(this.confirmationText))
        this.confirmationText = ''

        modal
            .on('accept', function() {
                this.clearBoard()
                this.unlocked = false;
                socket.emit('restart-game', this.room)
            }.bind(this))
            .on('reject', function() {
                socket.emit('user-left', this.room)
            }.bind(this))
    }

    Rules.fn.setPosition = function(element, type) {
        element.attr('data-type', type)
        element.html(type)
    }

    Rules.fn.iWon = function(type) {
        var deferred = $.Deferred();

        myMarkings = this.board.find("div[data-type='" + type + "']")
        myPositions = []

        $.each(myMarkings, function(i, element) {
            myPositions.push($(element).attr('class'))
        })

        deferred.resolve(this.probableVictories(myPositions))

        return deferred.promise()
    }

    Rules.fn.isEmpty = function(element) {
        return element.data('type') == 'undefined'
    }

    Rules.fn.probableVictories = function(positions) {
        var status = ($.inArray('a1', positions) != -1 &&
            $.inArray('b1', positions) != -1 &&
            $.inArray('c1', positions) != -1) ||

        ($.inArray('a2', positions) != -1 &&
            $.inArray('b2', positions) != -1 &&
            $.inArray('c2', positions) != -1) ||

        ($.inArray('a3', positions) != -1 &&
            $.inArray('b3', positions) != -1 &&
            $.inArray('c3', positions) != -1) ||

        ($.inArray('a1', positions) != -1 &&
            $.inArray('a2', positions) != -1 &&
            $.inArray('a3', positions) != -1) ||

        ($.inArray('b1', positions) != -1 &&
            $.inArray('b2', positions) != -1 &&
            $.inArray('b3', positions) != -1) ||

        ($.inArray('c1', positions) != -1 &&
            $.inArray('c2', positions) != -1 &&
            $.inArray('c3', positions) != -1) ||

        ($.inArray('a1', positions) != -1 &&
            $.inArray('b2', positions) != -1 &&
            $.inArray('c3', positions) != -1) ||

        ($.inArray('a3', positions) != -1 &&
            $.inArray('b2', positions) != -1 &&
            $.inArray('c1', positions) != -1);

        return status
    }
})