Module("VelhaMania.SetPoints", function(SetPoints) {
    SetPoints.fn.initialize = function() {
        this.names = $('#points').find('span.name')
    };

    SetPoints.fn.update = function(nickname, options) {
        var wins = 0,
            draws = 0,
            loss = 0;

        if (!options.win) {
            wins = 0

        } else {
            wins = options.win

        }
        if (!options.draw) {
            draws = 0

        } else {
            draws = options.draw

        }
        if (!options.lose) {
            loss = 0
        } else {
            loss = options.lose

        }

        if (_.isObject(nickname)) {
            nickname = _.keys(nickname)
            nickname = _.first(nickname)
        }

        var options = []
        $.each(this.names, function(i, name) {
            selectorWins = $(name).next().find('li.wins span.point')
            selectorDraws = $(name).next().find('li.draws span.point')
            selectorLoss = $(name).next().find('li.loss span.point')

            pointsDraw = +selectorDraws.text() + parseInt(draws)
            if ($(name).text() == nickname) {
                pointsWin = +selectorWins.text() + parseInt(wins)
                pointsLoss = +selectorLoss.text() + parseInt(loss)
            } else {
                pointsWin = +selectorWins.text() + parseInt(loss)
                pointsLoss = +selectorLoss.text() + parseInt(wins)
            }

            options.push({
                pointsWin: pointsWin,
                pointsDraw: pointsDraw,
                pointsLoss: pointsLoss
            })
        }.bind(this))

        this.changeStatus(options)
    }

    SetPoints.fn.changeStatus = function(options) {
        $.each(this.names, function(i, name) {
            selectorWins = $(name).next().find('li.wins span.point')
            selectorDraws = $(name).next().find('li.draws span.point')
            selectorLoss = $(name).next().find('li.loss span.point')

            selectorWins.text(options[i].pointsWin)
            selectorDraws.text(options[i].pointsDraw)
            selectorLoss.text(options[i].pointsLoss)
        })
    }

    SetPoints.fn.stats = function(options) {
        itsMe = _.first(_.keys(options.itsMe))
        opponent = _.first(_.keys(options.opponent))

        var response = $.ajax({
            url: "/stats-game/",
            type: "POST",
            data: {
                itsMe: itsMe,
                opponent: opponent
            },
        });

        response.success(function(data) {
            options = []

            $.each([itsMe, opponent], function(i, element) {
                if (data.disputes.opponent != element) {
                    options.push({
                        pointsDraw: data.disputes.draw,
                        pointsLoss: data.disputes.lose,
                        pointsWin: data.disputes.win
                    })
                } else {
                    options.push({
                        pointsDraw: data.disputes.draw,
                        pointsLoss: data.disputes.win,
                        pointsWin: data.disputes.lose
                    })
                }
            }.bind(this))

            this.changeStatus(options)

        }.bind(this))
    }
})